# Markovian

A CLI program inspired by MarkovJunior.

    racket racket/marko [--shape 80,50] Grid.csv

Some example rules are in *.csv.


## Vendors

v/palette.xml from https://github.com/mxgmn/MarkovJunior/blob/main/resources/palette.xml

# ENDN
