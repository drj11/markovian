# Notes on Racket

    brew install racket
    racket

    # to run a program:
    racket marko

You really do need the `#lang racket` declaration in the racket source file.

structures defined with
https://docs.racket-lang.org/guide/define-struct.html

use `.rkt` extension.

Documentation is quite good.

# END
