#! /usr/bin/env racket
#lang racket

;; https://docs.racket-lang.org/reference/Command-Line_Parsing.html
(require racket/cmdline)
;; https://docs.racket-lang.org/draw/index.html
(require racket/draw)
(require racket/format)
(require racket/random)

;; https://docs.racket-lang.org/file/gif.html
(require file/gif)

"Hello, I'm marko!"

(struct Rule (kind left right) #:inspector #f)

(define (read-rules in)
  (for/list ([row (in-lines in)])
    (let ([cells (regexp-split "," row)])
      (Rule (first cells) (second cells) (third cells)))))

(define (sort-rules rs)
  "sort the rules into (init regular)"
  (let ([inits '()]
        [regs '()])
      (for ([rule rs])
        (if (equal? (Rule-kind rule) "init")
          (set! inits (cons rule inits))
          (set! regs (cons rule regs))))
      (list (reverse inits) (reverse regs))))


(define East '(1 0))
(define North '(0 1))
(define West '(-1 0))
(define South '(0 -1))

(define (adj p dir)
  (list (+ (first p) (first dir))
        (+ (second p) (second dir))))


(struct World (grid w h))

; make a 2-dimensional world of (w h) (width by height) cells.
(define (make-world shape)
  (let* (
    [w (first shape)]
    [h (second shape)]
    [g (make-vector (* w h) #\B)])
    (World g w h)))

(define (in-world w p)
  (let ([x (first p)]
        [y (second p)])
    (and (<= 0 x) (< x (World-w w)) (<= 0 y) (< y (World-h w)))))

(define (World-at w p)
  "get the item at position p (list x y) in the World w"
  (let* ([x (first p)]
         [y (second p)]
         [i (+ (* (World-w w) y) x)])
    (cond
     [(or (< x 0) (<= (World-w w) x) (< y 0) (<= (World-h w) y)) "Z"]
     [else (vector-ref (World-grid w) i)])))


(define (world->bytes w)
  "Convert to byte sequence suitable for gif-add-image."
  (list->bytes (for/list ([ch (World-grid w)])
    (char->integer ch))))


(define (ascii-world w)
  (for ([row (in-range 0 (World-h w))])
    (for ([col (in-range 0 (World-w w))])
      (printf "~a" (World-at w (list col row))))
    (newline)))

(define (rgb-of ch)
  "Return (r g b) list."
  (case ch
    [(#\B) '(#x00 #x00 #x00)] ;; Black
    [(#\I) '(#x1D #x2B #x53)] ;; Indigo
    [(#\P) '(#x7E #x25 #x53)] ;; Purple
    [(#\E) '(#x00 #x87 #x51)] ;; Emerald
    [(#\N) '(#xAB #x52 #x36)] ;; browN
    [(#\D) '(#x5F #x57 #x4F)] ;; Dead, Dark
    [(#\A) '(#xC2 #xC3 #xC7)] ;; Alive, grAy
    [(#\W) '(#xFF #xF1 #xE8)] ;; White
    [(#\R) '(#xFF #x00 #x4D)] ;; Red
    [(#\O) '(#xFF #xA3 #x00)] ;; Orange
    [(#\Y) '(#xFF #xEC #x27)] ;; Yellow
    [(#\G) '(#x00 #xE4 #x36)] ;; Green
    [(#\U) '(#x29 #xAD #xFF)] ;; blUe
    [(#\S) '(#x83 #x76 #x9C)] ;; Slate
    [(#\K) '(#xFF #x77 #xA8)] ;; pinK
    [(#\F) '(#xFF #xCC #xAA)] ;; Fawn
      
    [(#\b) '(#x29 #x18 #x14)] ;; black
    [(#\i) '(#x11 #x1d #x35)] ;; indigo
    [(#\p) '(#x42 #x21 #x36)] ;; purple
    [(#\e) '(#x12 #x53 #x59)] ;; emerald
    [(#\n) '(#x74 #x2f #x29)] ;; brown
    [(#\d) '(#x49 #x33 #x3b)] ;; dead, dark
    [(#\a) '(#xa2 #x88 #x79)] ;; alive, gray
    [(#\w) '(#xf3 #xef #x7d)] ;; white
    [(#\r) '(#xbe #x12 #x50)] ;; red
    [(#\o) '(#xff #x6c #x24)] ;; orange
    [(#\y) '(#xa8 #xe7 #x2e)] ;; yellow
    [(#\g) '(#x00 #xb5 #x43)] ;; green
    [(#\u) '(#x06 #x5a #xb5)] ;; blue
    [(#\s) '(#x75 #x46 #x65)] ;; slate
    [(#\k) '(#xff #x6e #x59)] ;; pink
    [(#\f) '(#xff #x9d #x81)] ;; fawn
      
    [(#\C) '(#x00 #xff #xff)] ;; Cyan
    [(#\c) '(#x5f #xcd #xe4)] ;; cyan
    [(#\H) '(#xe4 #xbb #x40)] ;; Honey
    [(#\h) '(#x8a #x6f #x30)] ;; honey
    [(#\J) '(#x4b #x69 #x2f)] ;; Jungle
    [(#\j) '(#x45 #x10 #x7e)] ;; jungle
    [(#\L) '(#x84 #x7e #x87)] ;; Light
    [(#\l) '(#x69 #x6a #x6a)] ;; light
    [(#\M) '(#xff #x00 #xff)] ;; Magenta
    [(#\m) '(#x9c #x09 #xcc)] ;; magenta
    [(#\Q) '(#x9b #xad #xb7)] ;; aQua
    [(#\q) '(#x3f #x3f #x74)] ;; aqua
    [(#\T) '(#x37 #x94 #x6e)] ;; Teal
    [(#\t) '(#x32 #x3c #x39)] ;; teal
    [(#\V) '(#x8f #x97 #x4a)] ;; oliVe
    [(#\v) '(#x52 #x4b #x24)] ;; olive
    [(#\X) '(#xff #x00 #x00)] ;; X
    [(#\x) '(#xd9 #x57 #x63)] ;; x
    [(#\Z) '(#xff #xff #xff)] ;; Z
    [(#\z) '(#xcb #xdb #xfc)] ;; z
    [else #f]
))

(define (cmap)
  (for/list ([ch (in-range 0 256)])
    (let ([col (rgb-of (integer->char ch))])
      (if col (list->vector col) '#(#x00 #x00 #x00)))))

(define (ansi-color ch)
  "Return string for ANSI code to change to color for cell character ch"
  (let* ([rgb (rgb-of ch)]
      [out (open-output-bytes)])
    (fprintf out "\033[48;2;~a;~a;~am" (first rgb) (second rgb) (third rgb))
    (get-output-bytes out)))

(define (ansi-world w)
  "ANSI printer"
  (printf "\033[~aA" (World-h w))
  (for ([row (in-range 0 (World-h w))])
    (for ([col (in-range 0 (World-w w))])
      (let ([ch (World-at w (list col row))])
        (printf "~a~a" (ansi-color ch) ch)))
    (printf "\033[m~n")))

(define (world->png world filename)
  "PNG output"
  (let* ([w (World-w world)]
    [h (World-h world)]
    [bm (make-bitmap w h #f)]
    [dc (new bitmap-dc% [bitmap bm])])
    (for* ([row (in-range 0 h)]
      [col (in-range 0 w)])
      (let* ([ch (World-at world (list col row))]
        [rgb (rgb-of ch)]
        [color (make-object color% (first rgb) (second rgb) (third rgb))])
        (send dc set-pixel col row color))
      (send bm save-file filename 'png))))

(define (World-set! w pos elem)
    (let* ([x (first pos)]
           [y (second pos)]
           [i (+ (* (World-w w) y) x)])
      (vector-set! (World-grid w) i elem)))
  

(define (World-replace w footprint items)
  "replace the footprint in World w with items. `footprint` is a sequence 
of [x y] positions."
  (cond [(equal? (length footprint) (string-length items))]
    [else (error "bad lengths")])
  (for (
    [pos footprint]
    [item items])
    (World-set! w pos item)))


(define (World-matches-at w target s)
  "true when the string s matches at the footprint target in World w"
    (for/and ([p target]
              [c s])
             (equal? (World-at w p) c)))
  

(define (footprint-at w l p dir)
  "Return a footprint, a list of positions, at position p
  of shape `l` (in this implementation the shape is 1 × l,
  l being the length) proceeding in direction `dir`.
  The footprint is restricted to the bounds of world `w`;
  #f is returned if the footprint would exceed the bounds."

  (let/ec return
    (let ([ps (list p)])
      (for/list ([i (in-range (- l 1))])
        (set! p (adj p dir)) 
        (cond ((not (in-world w p)) (return #f)))
        (set! ps (cons p ps)))
      ps)))


;; Sequence of all sub spaces of shape l.
;; In this version l is a scalar and the sub spaces
;; are all contiguous lines.
(define (world-n-pos w l)
  (filter identity
	  (for*/list ([row (in-range 0 (World-h w))]
	              [col (in-range 0 (World-w w))]
	              [dir (list East North West South)])
	    (let* ([p (list col row)]
	          [ps (footprint-at w l p dir)])
	      ps))))

(define (find-rule-matches w s)
  "Within World w find matches for the string s"
  (let* ([shape (string-length s)])
    (for/list ([target (world-n-pos w shape)]
          #:when (World-matches-at w target s))
      target)))

(define (apply-1-rule world rule)
  (let/ec return
    (let ([matches (find-rule-matches world (Rule-left rule))])
	    (cond [(zero? (length matches)) (return #f)])
	    (let ([target (random-ref matches)])
	      ; (printf "target ~a~n" target)
	      (World-replace world target (Rule-right rule))
	      #t))))

(define world-shape (make-parameter '(80 50)))
(define model-file (make-parameter #f))

(command-line
  #:once-each
    ("--shape" shape "Set the World shape (comma separated)"
      (world-shape (map string->number (string-split shape #rx"[, ]+"))))
  #:args
    (model-filename) (model-file model-filename)
    )

(printf "shape~a~n" (world-shape))

(define (run-world world all-rules gif)
  (let ([inits (first all-rules)]
        [rules (second all-rules)]
        [w (World-w world)]
        [h (World-h world)])
  ; do inits
    (for ([rule inits])
      (apply-1-rule world rule))
    (ansi-world world)
    (gif-add-image gif 0 0 w h #f #f (world->bytes world))
  ; do regular rules
    (let again ([frame 1])
      (let
        ([rule-hit (for/or ([rule rules])
                     (apply-1-rule world rule))]
         [frame$ (~a frame #:align 'right #:width 4 #:pad-string "0")])
        (ansi-world world)
        ; (world->png world (string-append basename "-" frame$ ".png"))
        ; (print (string-append basename "-" frame$ ".png"))
        (gif-add-control gif 'any #f 2 #f)
        (gif-add-image gif 0 0 w h #f #f (world->bytes world))
        (if rule-hit (again (add1 frame)) 'stopped)))))

(let* (
  [basename (regexp-replace #rx"[.][^.]*$" (model-file) "")]
  [in (open-input-file (model-file))]
  [rules (sort-rules (read-rules in))]
  [world (make-world (world-shape))]
  [gifout (open-output-file (string-append basename ".gif")
           #:exists 'replace)]
  [gif (gif-start gifout (World-w world) (World-h world) 0 (cmap))]
  )
    (printf "~a~n" rules)
    (ascii-world world)

    (run-world world rules gif)
    (world->png world (string-append basename "-stopped" ".png"))
    (close-output-port gifout))
